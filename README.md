Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

The Code is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg

Shield: [![CC BY-NC-ND 4.0][cc-by-nc-nd-shield]][cc-by-nc-nd]

The Hardware is licensed under a
[Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License][cc-by-nc-sa].


[![CC BY-NC-ND 4.0][cc-by-nc-nd-image]][cc-by-nc-nd]

[cc-by-nc-nd]: https://creativecommons.org/licenses/by-nc-nd/4.0/
[cc-by-nc-nd-image]: https://licensebuttons.net/l/by-nc-nd/4.0/88x31.png
[cc-by-nc-nd-shield]: https://img.shields.io/badge/License-CC%20BY--NC--ND%204.0-lightgrey.svg


# xKy

Version Français

**Contexte**

Le projet xky est un projet Open-Source visant à créer des passerelles auto-alimentée se connectant sur la prise TIC (TéléInfoClient) du Linky et permettant de renvoyer les données issues de celle-ci via plusieurs supports de communication (Wifi, Lora, Ethernet, MQTT, InfluxDB)

Ce repositorie sert de vitrine pour les versions les plus récentes

Les repositories de travail sont les suivants:

- La version Wifi du projet se trouve ici https://gricad-gitlab.univ-grenoble-alpes.fr/ferrarij/winky
- La version LoRa du projet se trouve ici https://gricad-gitlab.univ-grenoble-alpes.fr/ferrarij/LoKy
- La version Ethernet du projet se trouve ici https://gricad-gitlab.univ-grenoble-alpes.fr/ferrarij/eky

Plus d'explication sur le projet sur https://miniprojets.net/

La version initiale de cette passerelle a été crée par Ferrari Jérôme CNRS/UGA/G-INP – G2ELAB

English version

**Context**

The xky project is an Open-Source project aimed at creating a self-powered gateway connecting to the TIC (TeleInfoClient) socket of the Linky and allowing data from it to be sent via the several way of communication (Wifi, Lora, Ethernet, MQTT, InfluxDB)

- The WiFi version of the project can be found here https://gricad-gitlab.univ-grenoble-alpes.fr/ferrarij/winKy
- The LoRa version of the project can be found here https://gricad-gitlab.univ-grenoble-alpes.fr/ferrarij/LoKy
- The Ethernet version of the project can be found here https://gricad-gitlab.univ-grenoble-alpes.fr/ferrarij/eky

Explanations are at https://miniprojets.net

This End-device was initially created by Ferrari Jérôme CNRS/UGA/G-INP – G2ELAB

![CNRS-logo][]
![G2elab-logo][]
![UGA-logo][]
![GINP-logo][]


[GINP-logo]: https://www.grenoble-inp.fr/uas/alias1/LOGO/Grenoble-INP-Etablissement-120px.png
[CNRS-logo]: https://upload.wikimedia.org/wikipedia/fr/thumb/7/72/Logo_Centre_national_de_la_recherche_scientifique_%282023-%29.svg/langfr-130px-Logo_Centre_national_de_la_recherche_scientifique_%282023-%29.svg.png
[G2elab-logo]: https://g2elab.grenoble-inp.fr/uas/alias31/LOGO/G2Elab+logo.png
[UGA-logo]:https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1D0TxrJin49KlU9Oo263OgjsanU9vKQZhKJ9LZsMjxxggnCEKO7NDsN-rW6CEXGJbLEE&usqp=CAU
